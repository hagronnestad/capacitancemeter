/*  RCTiming_capacitance_meter
 *  Paul Badger 2008
 *  Demonstrates use of RC time constants to measure the value of a capacitor 
 *
 * Theory   A capcitor will charge, through a resistor, in one time constant, defined as T seconds where
 *    TC = R * C
 * 
 *    TC = time constant period in seconds
 *    R = resistance in ohms
 *    C = capacitance in farads (1 microfarad (ufd) = .0000001 farad = 10^-6 farads ) 
 *
 *    The capacitor's voltage at one time constant is defined as 63.2% of the charging voltage.
 *
 *  Hardware setup:
 *  Test Capacitor between common point and ground (positive side of an electrolytic capacitor  to common)
 *  Test Resistor between chargePin and common point
 *  220 ohm resistor between dischargePin and common point
 *  Wire between common point and analogPin (A/D input)
 */

#define analogPin      0          // analog pin for measuring capacitor voltage
#define chargePin      7         // pin to charge the capacitor - connected to one end of the charging resistor
#define dischargePin   8         // pin to discharge the capacitor
#define resistorValue  10000.0F   // change this to whatever resistor value you are using
                                  // F formatter tells compliler it's a floating point value

#include <Wire.h> 
#include <LiquidCrystal_I2C.h>

LiquidCrystal_I2C lcd(0x27, 2, 1, 0, 4, 5, 6, 7, 3, POSITIVE);  // Set the LCD I2C address


unsigned long startTime;
unsigned long elapsedTime;
float microFarads;                // floating point variable to preserve precision, make calculations
float nanoFarads;

void setup(){
  pinMode(chargePin, OUTPUT);     // set chargePin to output
  digitalWrite(chargePin, LOW);  
  
  lcd.begin(16,2);               // initialize the lcd 
  lcd.home();                   // go home
  lcd.print("CAPACITANCE METR");  
  lcd.setCursor(0, 1);
  lcd.print ("  by HAG/STIGI  ");
  delay(2000);
}

void loop(){
  digitalWrite(chargePin, HIGH);  // set chargePin HIGH and capacitor charging
  startTime = micros();

  while(analogRead(analogPin) < 648){       // 647 is 63.2% of 1023, which corresponds to full-scale voltage 
  }

  elapsedTime = micros() - startTime;
  microFarads = ((elapsedTime / 1000.0f) / resistorValue) * 1000.0f;
  
  if ((elapsedTime / 1000) == 0) {
    lcd.clear();
    lcd.print(" -+");
    lcd.setCursor(0, 1);
    lcd.print("INSERT CAPACITOR");
    
  } else {
    lcd.clear();
    lcd.print(elapsedTime / 1000);
    lcd.print(" ms");
    lcd.setCursor(0, 1);
    lcd.print(microFarads);
    lcd.print(" mF");
  }
 

  /* dicharge the capacitor  */
  digitalWrite(chargePin, LOW);             // set charge pin to  LOW 
  pinMode(dischargePin, OUTPUT);            // set discharge pin to output 
  digitalWrite(dischargePin, LOW);          // set discharge pin LOW 
  
  while(analogRead(analogPin) > 0){         // wait until capacitor is completely discharged
  }

  pinMode(dischargePin, INPUT);            // set discharge pin back to input
  
  delay(500);
} 
